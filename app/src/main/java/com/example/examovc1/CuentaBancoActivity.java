package com.example.examovc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CuentaBancoActivity extends AppCompatActivity {

    private EditText txtNumCuenta, txtNombre, txtBanco, txtSaldo, txtCantidad, txtNuevoSaldo;
    private Spinner spinnerMovimientos;
    private Button btnAplicar, btnRegresar, btnLimpiar;
    private CuentaBanco cuentaBanco;
    private boolean saldoInicialSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);

        // Inicializar vistas
        txtNumCuenta = findViewById(R.id.txtNumCuenta);
        txtNombre = findViewById(R.id.txtNombre);
        txtBanco = findViewById(R.id.txtBanco);
        txtSaldo = findViewById(R.id.txtSaldo);
        txtCantidad = findViewById(R.id.txtCantidad);
        txtNuevoSaldo = findViewById(R.id.txtNuevoSaldo);
        spinnerMovimientos = findViewById(R.id.spinnerMovimientos);
        btnAplicar = findViewById(R.id.btnAplicar);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        // Inicializar CuentaBanco con valores predeterminados
        cuentaBanco = new CuentaBanco("", "", "", 0.0f);

        // Configurar botón aplicar
        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!saldoInicialSet) {
                    try {
                        float saldoInicial = Float.parseFloat(txtSaldo.getText().toString());
                        cuentaBanco.setSaldo(saldoInicial);
                        saldoInicialSet = true;
                    } catch (NumberFormatException e) {
                        Toast.makeText(CuentaBancoActivity.this, "Por favor, ingrese un saldo válido.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                String movimiento = spinnerMovimientos.getSelectedItem().toString();
                float cantidad = 0.0f;

                if (!movimiento.equals("Consultar")) {
                    try {
                        cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    } catch (NumberFormatException e) {
                        Toast.makeText(CuentaBancoActivity.this, "Por favor, ingrese una cantidad válida.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if (movimiento.equals("Consultar")) {
                    txtNuevoSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                } else if (movimiento.equals("Retirar")) {
                    try {
                        cuentaBanco.retirarDinero(cantidad);
                        txtNuevoSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                    } catch (IllegalArgumentException error) {
                        Toast.makeText(CuentaBancoActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else if (movimiento.equals("Depositar")) {
                    cuentaBanco.hacerDeposito(cantidad);
                    txtNuevoSaldo.setText(String.valueOf(cuentaBanco.obtenerSaldo()));
                }
            }
        });

        // Configurar botón regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Configurar botón limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumCuenta.setText("");
                txtNombre.setText("");
                txtBanco.setText("");
                txtSaldo.setText("");
                txtCantidad.setText("");
                txtNuevoSaldo.setText("");
                saldoInicialSet = false;
            }
        });
    }
}
